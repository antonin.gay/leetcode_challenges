import pytest

from src.c1672_RichestCustomerWealth import Solution

test_data = [
    ([[1, 2, 3], [3, 2, 1]], 6),
    ([[1, 5], [7, 3], [3, 5]], 10),
    ([[2, 8, 7], [7, 1, 3], [1, 9, 5]], 17)
]


@pytest.mark.parametrize("mat, expected", test_data)
def test_richest_customer(mat, expected):
    assert Solution().maximumWealth(mat) == expected
