import pytest

from src.c0005_LongestPalyndrSubstring import Solution

test_data = [
    ("babad", "bab"),  # Can also be aba
    ("cbbd", "bb"),
    ("a", "a"),
]


@pytest.mark.parametrize("s, expected", test_data)
def test_richest_customer(s, expected):
    assert Solution().longestPalindrome(s) == expected
