import pytest

from src.c0234_Palindrome_Linked_List import ListNode, Solution

test_data = [
    ([1, 2, 2, 1], True),
    ([1, 1, 2, 1], False),
    ([1, 2, 3], False),
    ([1, 2, 1], True)
]


def create_linked_list(lst: list) -> ListNode:
    def create_node_i(i: int):
        if i == len(lst) - 1:
            return ListNode(val=lst[i])
        else:
            return ListNode(val=lst[i], _next=create_node_i(i + 1))

    head = create_node_i(0)
    return head


@pytest.mark.parametrize("lst, expected", test_data)
def test_palindrome(lst, expected):
    head = create_linked_list(lst)
    assert Solution().isPalindrome(head) == expected
