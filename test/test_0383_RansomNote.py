import pytest

from src.c0383_RansomNote import Solution

test_data = [
    ("a", "b", False),
    ("aa", "ba", False),
    ("aa", "aab", True)
]


@pytest.mark.parametrize("ransome_note, magazine, expected", test_data)
def test_ransom_note(ransome_note, magazine, expected):
    assert Solution().canConstruct(ransome_note, magazine) == expected
