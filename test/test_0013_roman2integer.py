import pytest

from src.c0013_roman2integer import Solution

test_data = [
    ("III", 3),
    ("LVIII", 58),
    ("IV", 4),
    ("MCMXCIV", 1994)
]


@pytest.mark.parametrize("s, expected", test_data)
def test_roman2integer(s, expected):
    assert Solution().romanToInt(s) == expected
