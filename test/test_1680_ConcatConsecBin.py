import pytest

from src.c1680_ConcatConsecBin import Solution

test_data = [
    (1, 1),
    (3, 27),
    (12, 505379714),
    (42, 727837408),
    (34199, 134189130)
]


@pytest.mark.parametrize("x, expected", test_data)
def test_richest_customer(x, expected):
    assert Solution().concatenatedBinary(x) == expected
