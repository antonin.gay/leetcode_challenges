import pytest

from src.c1342_NumberOfStepsToZero import Solution

test_data = [
    (14, 6),
    (8, 4),
    (123, 12)
]


@pytest.mark.parametrize("n, expected", test_data)
def test_num_steps_to_zero(n, expected):
    assert Solution().numberOfSteps(n) == expected
