import pytest

from src.c0002_AddTwoNumbers import Solution, ListNode

test_data = [
    ([2, 4, 3], [5, 6, 4], [7, 0, 8]),
    ([0], [0], [0]),
    ([9, 9, 9, 9, 9, 9, 9], [9, 9, 9, 9], [8, 9, 9, 9, 0, 0, 0, 1]),
]


def create_linked_list(lst: list) -> ListNode:
    def create_node_i(i: int):
        if i == len(lst) - 1:
            return ListNode(val=lst[i])
        else:
            return ListNode(val=lst[i], _next=create_node_i(i + 1))

    head = create_node_i(0)
    return head


def linked_list_to_list(head: ListNode) -> list:
    node = head
    res = []
    while node is not None:
        res.append(node.val)
        node = node.next

    return res


@pytest.mark.parametrize("n1, n2, expected", test_data)
def test_add2numbers(n1, n2, expected):
    head1 = create_linked_list(n1)
    head2 = create_linked_list(n2)

    assert linked_list_to_list(Solution().addTwoNumbers(head1, head2)) == expected
