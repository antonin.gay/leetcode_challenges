import pytest

from src.c0007_ReverseInteger import Solution

test_data = [
    (123, 321),
    (-123, -321),
    (120, 21),
    (1, 1),
    (1534236469, 0),
]


@pytest.mark.parametrize("x, expected", test_data)
def test_richest_customer(x, expected):
    assert Solution().reverse(x) == expected
