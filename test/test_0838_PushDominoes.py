import pytest

from src.c0838_PushDominoes import Solution

test_data = [
    ("RR.L", "RR.L"),
    (".L.R...LR..L..", "LL.RR.LLRRLL.."),
]


@pytest.mark.parametrize("n, expected", test_data)
def test_num_steps_to_zero(n, expected):
    assert Solution().pushDominoes(n) == expected
