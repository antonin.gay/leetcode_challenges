import pytest

from src.c0014_LongestPrefix import Solution

test_data = [
    (["flower","flow","flight"], "fl"),
    (["dog","racecar","car"], ""),
]


@pytest.mark.parametrize("s, expected", test_data)
def test_richest_customer(s, expected):
    assert Solution().longestCommonPrefix(s) == expected
