import pytest
from src.c0001_TwoSum import Solution

test_data = [
    ([2, 7, 11, 15], 9, [0, 1]),
    ([3, 2, 4], 6, [1, 2]),
    ([3, 3], 6, [0, 1]),
    ([3, 2, 3], 6, [0, 2])
]


@pytest.mark.parametrize("nums, target, expected", test_data)
def test_ransom_note(nums, target, expected):
    res = Solution().twoSum(nums, target)
    assert res == expected or [res[1], res[0]] == expected
