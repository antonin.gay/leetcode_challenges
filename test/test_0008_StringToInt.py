import pytest

from src.c_0008_StringToInt import Solution

test_data = [
    ("123", 123),
    ("-123", -123),
    ("00120", 120),
    ("+1", 1),
    ("21534236469", 2 ** 31 - 1),
    ("words and 987", 0),
    ("+-12", 0),
    ("", 0),
]


@pytest.mark.parametrize("x, expected", test_data)
def test_my_atoi(x, expected):
    assert Solution().myAtoi(x) == expected
