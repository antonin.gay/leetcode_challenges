import pytest

from src.c0004_MedianTwoArrays import Solution

test_data = [
    ([1, 3], [2], 2),
    ([1, 2], [3, 4], 2.5),
    ([1, 3], [2, 4], 2.5),
]


@pytest.mark.parametrize("nums1, nums2, expected", test_data)
def test_richest_customer(nums1, nums2, expected):
    assert Solution().findMedianSortedArrays(nums1, nums2) == expected
