import pytest

from src.c0412_FizzBuzz import Solution

test_data = [
    (3, ["1", "2", "Fizz"]),
    (5, ["1", "2", "Fizz", "4", "Buzz"]),
    (15, ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz"])
]


@pytest.mark.parametrize("n, expected", test_data)
def test_fizz_buzz(n, expected):
    assert Solution().fizzBuzz(n) == expected
