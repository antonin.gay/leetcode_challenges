import pytest

from src.c0003_LongestSubstring import Solution

test_data = [
    ("abcabcbb", 3),
    ("bbbbb", 1),
    ("pwwkew", 3),
    ("", 0),
    ("bwf", 3),
    ("aab", 2),
    ("cdd", 2),
    ("abcb", 3),
]


@pytest.mark.parametrize("s, expected", test_data)
def test_richest_customer(s, expected):
    assert Solution().lengthOfLongestSubstring(s) == expected
