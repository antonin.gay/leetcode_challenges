import pytest

from src.c0990_SatisfiabilityEquations import Solution

test_data = [
    (["a==b", "b!=a"], False),
    (["b==a", "a==b"], True),
    (["a==b", "b!=c", "c==a"], False),
    (["a!=a"], False),
]


@pytest.mark.parametrize("lst, expected", test_data)
def test_richest_customer(lst, expected):
    assert Solution().equationsPossible(lst) == expected
