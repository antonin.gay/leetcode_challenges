# https://leetcode.com/problems/fizz-buzz/

# Given an integer n, return a string array answer (1-indexed) where:
#     answer[i] == "FizzBuzz" if i is divisible by 3 and 5.
#     answer[i] == "Fizz" if i is divisible by 3.
#     answer[i] == "Buzz" if i is divisible by 5.
#     answer[i] == i (as a string) if none of the above conditions are true.


from typing import List


class Solution:
    def fizzBuzz(self, n: int) -> List[str]:
        ret = ["0"] * n
        for i in range(1, n + 1):
            # We're working with i starting at 1
            div3: bool = (i / 3 == i // 3)  # True if divisible by 3
            div5: bool = (i / 5 == i // 5)  # True if divisible by 5

            if div3:
                if div5:
                    # if div3 and div5
                    res = "FizzBuzz"
                else:
                    # if div3 but not div5
                    res = "Fizz"
            elif div5:
                # if div5 but not div3
                res = "Buzz"
            else:
                # if None
                res = str(i)

            ret[i - 1] = res

        return ret
