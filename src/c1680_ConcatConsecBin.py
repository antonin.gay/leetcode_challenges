class Solution:
    def concatenatedBinary(self, n: int) -> int:
        s = "0b"
        for i in range(1, n + 1):
            s += bin(i)[2:]

        res = int(s, 2)

        return res % (10 ** 9 + 7)
