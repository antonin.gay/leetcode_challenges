from typing import List


class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        i1 = 0
        i2 = 0

        tot_len = len(nums1) + len(nums2)
        new_list = [0] * tot_len
        while i1 + i2 <= tot_len / 2:
            try:
                if nums1[i1] < nums2[i2]:
                    new_list[i1 + i2] = nums1[i1]
                    i1 += 1
                else:
                    new_list[i1 + i2] = nums2[i2]
                    i2 += 1
            except IndexError as e:
                if i1 >= len(nums1):
                    new_list[i1 + i2] = nums2[i2]
                    i2 += 1
                elif i2 >= len(nums2):
                    new_list[i1 + i2] = nums1[i1]
                    i1 += 1
                else:
                    raise e

        if tot_len % 2:
            return new_list[i1 + i2 - 1]
        else:
            return (new_list[i1 + i2 - 1] + new_list[i1 + i2 - 2]) / 2
