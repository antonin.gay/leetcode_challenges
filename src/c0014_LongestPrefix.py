from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        cpt = 0

        try:
            while True:
                c = strs[0][cpt]
                for s in strs:
                    if s[cpt] != c:
                        return s[:cpt]
                cpt += 1
        except IndexError:
            return strs[0][:cpt]
