from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        # noinspection PyTypeChecker
        argsort: List[int] = sorted(range(len(nums)), key=nums.__getitem__)
        nums.sort()
        i = 0
        i_end = len(nums) - 1
        while i < i_end:
            _sum = nums[i] + nums[i_end]
            if _sum > target:
                i_end -= 1
            elif _sum < target:
                i += 1
            elif _sum == target:
                return [argsort[i], argsort[i_end]]
