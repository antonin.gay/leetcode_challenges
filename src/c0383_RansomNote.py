class Solution:

    def canConstruct(self, ransom_note: str, magazine: str) -> bool:
        # For each word, we create a dictionnary of letter counts
        rn_dict = self.letter_count(ransom_note)
        mag_dict = self.letter_count(magazine)

        # For each letter of ransomNote, we verify if there are enough occurence in magazine
        for key, count in rn_dict.items():
            try:
                if mag_dict[key] < count:
                    return False
            except KeyError:
                return False

        return True

    @staticmethod
    def letter_count(word) -> dict:
        rn_dict = dict()
        for c in word:
            try:
                rn_dict[c] += 1
            except KeyError:
                # If the letter isn't already in the dict, we create the entry
                rn_dict[c] = 1
        return rn_dict
