class Solution:
    def pushDominoes(self, dominoes: str) -> str:
        res: str = ""
        dots_len: int = 0
        l_force = "L"

        # We go through the whole domino chain, adding a "ghost" R domino at the end so the whole line is treated
        for i, c in enumerate(dominoes + "R"):
            # If the current char is a dot, we skip until a force is met
            if c == ".":
                dots_len += 1

            else:  # c == "R" or "L"
                # If the char is a force,
                if dots_len != 0:
                    # We got dots between i_init and i (excluded) we must complete their state
                    r_force = c

                    new_str = self.simulate_between_two_forces(dots_len, l_force, r_force)

                    res += new_str
                dots_len = 0
                l_force = c
                res += c

        return res[:-1]

    @staticmethod
    def simulate_between_two_forces(dots_len, l_force, r_force):
        """ Determine the result of two forces applied to a line of still dominoes

        :param dots_len: Len of dots (still dominoes)
        :param l_force: Force applied to the left
        :param r_force: Force applied to the right
        :return: The values of the still dominoes
        """
        res = ""

        # First half is "R" if left force is "R", else is "L" if right force is "L" else is still "."
        for _ in range(dots_len // 2):
            if l_force == "R":
                res += "R"
            elif r_force == "L":
                res += "L"
            else:
                res += "."

        # Middle point, if exists, is "." if R+L, or R or L otherwise
        if dots_len % 2 != 0:
            if l_force == "R":
                if r_force == "L":
                    res += "."
                else:
                    res += "R"
            else:
                if r_force == "L":
                    res += "L"
                else:
                    res += "."

        # Second half is "L" if right force is "L", else is "R" if left force is "R" else is still "."
        for _ in range(dots_len // 2):
            if r_force == "L":
                res += "L"
            elif l_force == "R":
                res += "R"
            else:
                res += "."
        return res
