from typing import List


class Solution:
    def equationsPossible(self, equations: List[str]) -> bool:
        equal_dict: dict = {}

        # We create for each letter a list of equal other letters
        for equation in equations:
            if equation[1:3] == "==":
                var1 = equation[0]
                var2 = equation[3]

                equal_dict[var1] = equal_dict.get(var1, []) + [var2]
                equal_dict[var2] = equal_dict.get(var2, []) + [var1]

        # We will recursively create clusters by crawling through the lists
        clust_dict = {}

        def recursive(letter, cluster_id):
            if clust_dict.get(letter, None) is None:
                clust_dict[letter] = cluster_id
                for next_letter in equal_dict.get(letter, []):
                    recursive(next_letter, cluster_id)

        for i, var in enumerate(equal_dict.keys()):
            if clust_dict.get(var, None) is None:
                recursive(var, i)

        # Then we assert that no inequality are in the same cluster
        for equation in equations:
            if equation[1:3] == "!=":
                var1 = equation[0]
                var2 = equation[3]

                if var1 == var2:
                    return False
                if clust_dict.get(var1, -1) == clust_dict.get(var2, -2):
                    return False

        return True
