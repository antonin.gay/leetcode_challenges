class Solution:
    dict_numb: dict = {f"{i}": i for i in range(10)}

    def myAtoi(self, s: str) -> int:
        if s == "":
            return 0

        neg: bool = False
        y = 0
        i = 0
        while s[i] == " ":
            i += 1
            if i == len(s):
                return 0

        if s[i] == "-" or s[i] == "+":
            neg = s[i] == "-"
            i += 1

        while i < len(s):
            c = s[i]
            if c in self.dict_numb.keys():
                y *= 10
                y += self.dict_numb[c]
            else:
                print(f"Non-digit \"{c}\" seen")
                break
            i += 1

        if neg:
            y *= -1

        if y > 2 ** 31 - 1:
            y = 2 ** 31 - 1
        if y < -2 ** 31:
            y = -2 ** 31

        return y

        # bisoux je taime !
