class Solution:
    def numberOfSteps(self, num: int) -> int:
        cpt = 0

        while num != 0:
            if num % 2 == 0:
                num /= 2
            else:
                num -= 1
            cpt += 1

        return cpt
