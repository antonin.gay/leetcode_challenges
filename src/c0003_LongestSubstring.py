class Solution:

    def lengthOfLongestSubstring(self, s: str) -> int:
        last_idx: dict = {}
        length: int = 0
        max_length: int = 0

        for i, c in enumerate(s):
            if last_idx.get(c, None) is None:
                length += 1
            else:
                length = min(length + 1, i - last_idx[c])
            last_idx[c] = i
            max_length = max(length, max_length)

        return max_length
