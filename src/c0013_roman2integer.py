class Solution:
    # List of values of each roman letter
    values: dict = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}

    def romanToInt(self, s: str) -> int:
        res = 0

        while len(s) != 0:
            # We go through the string from left to right, removing the first letter as the current number to add
            c = s[0]
            s = s[1:]

            # We add the value of the current selected letter
            res += self.values[c]

            # If it was not the last letter, we verify if we are in one of the case where it should have been removed:
            # There are six instances where subtraction is used:
            #   I can be placed before V (5) and X (10) to make 4 and 9.
            #   X can be placed before L (50) and C (100) to make 40 and 90.
            #   C can be placed before D (500) and M (1000) to make 400 and 900.
            # In those cases, we remove it twice, to compensate the previous line which was adding it by default

            if len(s) != 0:
                match c:
                    case "I":
                        if s[0] == "V" or s[0] == "X":
                            res -= 2 * self.values[c]
                    case "X":
                        if s[0] == "L" or s[0] == "C":
                            res -= 2 * self.values[c]
                    case "C":
                        if s[0] == "D" or s[0] == "M":
                            res -= 2 * self.values[c]

        return res
