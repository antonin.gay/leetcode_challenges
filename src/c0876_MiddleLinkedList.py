# Definition for singly-linked list.
from typing import Optional


class ListNode:
    def __init__(self, val=0, _next=None):
        self.val = val
        self.next = _next


class Solution:
    def middleNode(self, head: Optional[ListNode]) -> Optional[ListNode]:
        def recursive(node: ListNode, node_idx: int) -> (int, ListNode):
            # If it is the last Node, returns the middle index
            # If it is the middle Node (based on returned index) returns node
            # Otherwise, just pass through

            if node is None:
                last_index = node_idx
                middle_index = last_index // 2
                return middle_index, None
            else:
                middle_index, passed_node = recursive(node.next, node_idx + 1)
                if middle_index == node_idx:
                    passed_node = node
                return middle_index, passed_node

        _, middle_node = recursive(head, 0)

        return middle_node
