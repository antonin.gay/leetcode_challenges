from typing import List


# todo: not efficient code, could be optimised and improved
class Solution:
    def kWeakestRows(self, mat: List[List[int]], k: int) -> List[int]:
        cpt = 0
        res = [-1] * k

        # We go through the matrix, column by column, and add the lines with zeros to the list
        for j in range(len(mat[0])):
            for i in range(len(mat)):
                if i in res:
                    continue
                if mat[i][j] == 0:
                    res[cpt] = i
                    cpt += 1
                    if cpt == k:
                        return res

        # For lines with only 1, we deal with them after
        for i in range(len(mat)):
            if i not in res:
                res[cpt] = i
                cpt += 1
                if cpt == k:
                    return res
