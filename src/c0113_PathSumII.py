from typing import Optional, List


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def pathSum(self, root: Optional[TreeNode], targetSum: int, current_path: List[int] = None) -> List[List[int]]:
        if root is None:
            return []
        new_path: List[int] = (current_path if current_path else []) + [root.val]
        if root.val == targetSum and root.left is None and root.right is None:
            return [new_path]

        return [*self.pathSum(root.left, targetSum - root.val, new_path),
                *self.pathSum(root.right, targetSum - root.val, new_path)]
