# Definition for singly-linked list.
from typing import Optional


class ListNode:
    def __init__(self, val=0, _next=None):
        self.val = val
        self.next = _next


class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        def recursive(node1: ListNode, node2: ListNode, offset: int = 0) -> ListNode:
            node1 = node1 if node1 is not None else ListNode(0, None)
            node2 = node2 if node2 is not None else ListNode(0, None)

            val = node1.val + node2.val + offset
            next_offset = val // 10

            if node1.next is None and node2.next is None:
                if next_offset != 0:
                    next_node = ListNode(val=next_offset, _next=None)
                else:
                    next_node = None
            else:
                next_node = recursive(node1.next, node2.next, next_offset)

            return ListNode(val=val % 10, _next=next_node)

        return recursive(l1, l2)
