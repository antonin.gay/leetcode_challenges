class Solution:
    def longestPalindrome(self, s: str) -> str:
        idx_lists: dict = {}
        longest: str = s[0]

        for i, c in enumerate(s):
            if idx_lists.get(c, None) is None:
                idx_lists[c] = [i]
            else:
                for j in idx_lists.get(c, None):
                    if i - j + 1 > len(longest):
                        i1 = j
                        i2 = i

                        # Going through the word from end and begin, comparing letters to see if palyndrom
                        palyndrom: bool = True
                        while i1 <= i2:
                            if s[i1] != s[i2]:
                                palyndrom = False
                                break
                            i1 += 1
                            i2 -= 1

                        if palyndrom:
                            longest = s[j:i + 1]

                idx_lists[c] = idx_lists[c] + [i]

        return longest
