class Solution:
    def reverse(self, x: int) -> int:
        neg = x < 0
        if neg:
            x *= -1

        order = 0
        while x // 10 ** (order + 1) != 0:
            order += 1

        y = 0
        for i in range(order + 1):
            y += (x // (10 ** (order - i))) * 10 ** i
            x -= (x // (10 ** (order - i))) * (10 ** (order - i))

        if neg:
            y *= -1

        # todo: this line is cheating considering the challenge description. Should be replaced.
        if not -2 ** 31 < y < 2 ** 31 - 1:
            y = 0

        return y
