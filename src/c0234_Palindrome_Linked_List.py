# Definition for singly-linked list.
from typing import Optional


class ListNode:
    def __init__(self, val=0, _next=None):
        self.val = val
        self.next = _next


class Solution:
    def isPalindrome(self, head: Optional[ListNode]) -> bool:
        unlinked_list = []
        node = head

        # We create an unlinked list from the linked list.
        # todo: This solution feels like cheating, there might be faster
        while node is not None:
            unlinked_list.append(node.val)
            node = node.next

        # Then we go through half of the list and compare each value to the opposite one.
        # If we find a difference, we return False, otherwise we return True
        length = len(unlinked_list)
        for i in range(length // 2 + 1):
            if unlinked_list[i] != unlinked_list[length - 1 - i]:
                return False

        return True
