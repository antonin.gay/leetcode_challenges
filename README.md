# LeetCode challenges

Antonin GAY - [antonin.gay@gmail.com](mailto:antonin.gay@gmail.com)

This project compiles all the code produced to complete leetcode challenges.

Each challenge done has a corresponding file is the src/ folder, and a pytest file in the test/ folder.
